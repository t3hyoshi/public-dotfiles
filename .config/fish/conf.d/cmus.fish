function cmus-d --wraps cmus --description "Launches cmus and the rpc wrapper"
	$HOME/cmus-rpc-rs/target/release/cmus-rpc-rs --link &> /dev/null & cmus
end
