call plug#begin('~/.config/nvim/plugs')
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vimwiki/vimwiki'
Plug 'dracula/vim', { 'as': 'dracula' }
call plug#end()

colorscheme dracula
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let maplocalleader=";"
let g:limelight_conceal_ctermfg = 240

set noshowmode
set nocompatible
set number

nnoremap <Leader>h <C-w>h
nnoremap <Leader>j <C-w>j
nnoremap <Leader>k <C-w>k
nnoremap <Leader>l <C-w>l

au BufNewFile,BufRead /*.rasi setf css
filetype plugin on
syntax on
